FROM php:8.0-apache
LABEL maintainer="dave@hornigan.com"
LABEL author="dave@hornigan.com"

# Download script to install PHP extensions and dependencies
ADD https://raw.githubusercontent.com/mlocati/docker-php-extension-installer/master/install-php-extensions /usr/local/bin/

RUN set -ex
COPY content/etc/ /etc/
COPY content/usr/ /usr/

RUN chmod uga+x /usr/local/bin/install-php-extensions && sync

RUN DEBIAN_FRONTEND=noninteractive apt-get update -q \
    && DEBIAN_FRONTEND=noninteractive apt-get install -s -qq -y curl git zip unzip cron gnupg nano sudo \
        apt-transport-https \
        libmcrypt-dev libzip-dev libjpeg-dev libpng-dev \
        libxpm-dev libxslt-dev libicu-dev libldap2-dev libfreetype6-dev libmagickwand-dev libpq-dev \
        libmemcached-dev \
    && install-php-extensions bcmath bz2 exif gd intl ldap memcached mysqli opcache json xml \
      pdo_mysql pdo_pgsql pgsql redis xsl zip sockets  \
    && a2enmod rewrite expires headers

# install redis
#RUN yes '' | pecl install redis && docker-php-ext-enable redis
# install memcached
#RUN yes '' | pecl install memcached && docker-php-ext-enable memcached

ENV CONFD_VERSION=0.16.0
ADD https://github.com/kelseyhightower/confd/releases/download/v$CONFD_VERSION/confd-$CONFD_VERSION-linux-amd64 /usr/local/bin/confd
RUN chmod +x /usr/local/bin/confd

# Install Composer.
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir="/usr/local/bin" --filename="composer"
ENV PATH=$PATH:/root/composer/vendor/bin COMPOSER_ALLOW_SUPERUSER=1